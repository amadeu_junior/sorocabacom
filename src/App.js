import Global from "./styleGlobal";

import Home from "./pages/Home";

function App() {
  return (
    <>
      <Global />
      <Home />
    </>
  );
}

export default App;
