import React from "react";

import { Navbar, ImgTop, NavbarTitle } from "../../src/pages/Home/home.style";

import Imagem_3 from "../../src/img/Imagem_3.png";

export default function App() {
  return (
    <Navbar>
      <ImgTop src={Imagem_3} alt="" />
      <NavbarTitle>SUPERGIANTGAMES</NavbarTitle>
    </Navbar>
  );
}
