import React from "react";

import { useFormik } from "formik";

import * as Yup from "yup";

import {
  Form,
  BaseInput,
  WrapperInput,
  Input,
  TextArea,
  ErrorLabel,
  Button,
} from "../pages/Home/home.style";

export default function App() {
  const formik = useFormik({
    initialValues: {
      fullName: "",
      email: "",
      messageArea: "",
    },
    validationSchema: Yup.object({
      fullName: Yup.string()
        .max(50, "Máximo de 50 caracteres")
        .required("Obrigatório"),
      email: Yup.string()
        .max(100, "Máximo de 100 caracteres")
        .required("Obrigatório"),
      email: Yup.string()
        .email("Formato de e-mail inválido")
        .required("Obrigatório"),
      messageArea: Yup.string()
        .max(300, "Máximo de 300 caracteres")
        .required("Obrigatório"),
    }),
    onSubmit: (values, { resetForm }) => {
      // alert(JSON.stringify(values, null, 2));
      alert("Mensagem enviada com sucesso!");
      resetForm({ values: "" });
    },
  });

  return (
    <Form onSubmit={formik.handleSubmit}>
      <BaseInput>
        <WrapperInput>
          <Input
            type="text"
            placeholder="Nome"
            name="fullName"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.fullName}
          />
          <ErrorLabel>
            {formik.errors.fullName &&
              formik.touched.fullName &&
              formik.errors.fullName}
          </ErrorLabel>
        </WrapperInput>

        <WrapperInput>
          <Input
            type="e-mail"
            placeholder="E-mail"
            name="email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
          />
          <ErrorLabel>
            {formik.errors.email && formik.touched.email && formik.errors.email}
          </ErrorLabel>
        </WrapperInput>
      </BaseInput>
      <TextArea
        name=""
        id=""
        cols="30"
        rows="10"
        name="messageArea"
        placeholder="Menssagem"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.messageArea}
      />
      <ErrorLabel>
        {formik.errors.messageArea &&
          formik.touched.messageArea &&
          formik.errors.messageArea}
      </ErrorLabel>
      <Button>ENVIAR</Button>
    </Form>
  );
}
