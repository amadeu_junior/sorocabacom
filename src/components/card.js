import React from "react";

import {
  Card,
  ImgPena1,
  ImgPena2,
  ImgPena3,
  ImgPena4,
  ImgCard,
  CardText,
} from "../pages/Home/home.style";

import Pena1 from "../../src/img/pena1.png";
import Pena2 from "../../src/img/pena2.png";
import Pena3 from "../../src/img/pena3.png";
import Pena4 from "../../src/img/pena4.png";
import ImagemCard from "../../src/img/ImagemCard.png";

export default function App() {
  return (
    <Card>
      <ImgPena1 src={Pena1} alt="" />
      <ImgPena2 src={Pena2} alt="" />
      <ImgPena3 src={Pena3} alt="" />
      <ImgPena4 src={Pena4} alt="" />
      <ImgCard src={ImagemCard} alt="" />
      <CardText>
        "Olha, o que quer que você esteja pensando, me faça um favor, não
        solte."
      </CardText>
    </Card>
  );
}
