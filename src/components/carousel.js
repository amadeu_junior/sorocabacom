import React from "react";

import Carousel from "react-elastic-carousel";

import {
  Item,
  GroupPersona,
  ImgPersonage,
  CardPersonaSuperior,
  CardPersona,
  CardPersonaText,
} from "../pages/Home/home.style";

import Grant from "../../src/img/Grant.png";
import Red from "../../src/img/Red.png";
import Sybil from "../../src/img/Sybil_2.png";

export default function App() {
  const breakPoints = [
    // { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 1, itemsToScroll: 1 },
    { width: 780, itemsToShow: 2 },
    { width: 1200, itemsToShow: 3 },
  ];
  return (
    <Carousel breakPoints={breakPoints}>
      <Item>
        <GroupPersona>
          <ImgPersonage src={Grant} alt="" />
          <CardPersonaSuperior></CardPersonaSuperior>
          <CardPersona>
            <CardPersonaText>
              A Camerata foi apenas os dois no início, e suas fileiras nunca
              foram destinadas a exceder um número a ser contado em uma mão.
            </CardPersonaText>
          </CardPersona>
        </GroupPersona>
      </Item>

      <Item>
        <GroupPersona>
          <ImgPersonage src={Red} alt="" />
          <CardPersonaSuperior></CardPersonaSuperior>
          <CardPersona>
            <CardPersonaText>
              A Camerata foi apenas os dois no início, e suas fileiras nunca
              foram destinadas a exceder um número a ser contado em uma mão.
            </CardPersonaText>
          </CardPersona>
        </GroupPersona>
      </Item>

      <Item>
        <GroupPersona>
          <ImgPersonage src={Sybil} alt="" />
          <CardPersonaSuperior></CardPersonaSuperior>
          <CardPersona>
            <CardPersonaText>
              A Camerata foi apenas os dois no início, e suas fileiras nunca
              foram destinadas a exceder um número a ser contado em uma mão.
            </CardPersonaText>
          </CardPersona>
        </GroupPersona>
      </Item>
    </Carousel>
  );
}
