import styled from "styled-components";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import ImgCardCentral from "../../img/Imagem_2.png";

const ContainerFluid = styled.div`
  width: 100%;
  overflow-x: hidden;
`;

const Navbar = styled.div`
  background: #363636 0% 0% no-repeat;
  opacity: 1;

  width: 100%;
  height: 109px;
  align-items: center;
  justify-content: center;
  display: flex;
`;

const ImgTop = styled.img``;

const NavbarTitle = styled.span`
  font: bold 22.5px "Montserrat", sans-serif;
  color: #fff;
`;

const ItensFloat = styled.div`
  justify-content: center;
  display: flex;
`;

const Drag = styled.div`
  width: 28.89px;
  height: 47.82px;
  border-radius: 30px;
  background: #ffffff;
  position: absolute;
  margin-top: 1055px;
  z-index: 2;

  @media (max-width: 780px) {
    margin-top: 615px;
  }
`;

const Divisor = styled.div`
  font-size: 22px;
  justify-content: center;
  display: flex;
`;

const CardCentral = styled.div`
  background: url(${ImgCardCentral}) no-repeat;
  height: 1080px;

  justify-content: center;
  display: flex;

  @media (max-width: 780px) {
    opacity: 0.85;

    justify-content: center;
    display: flex;
    height: auto;
  }
`;

const Card = styled.div`
  background-image: linear-gradient(to bottom, #9c020480, #a9212363);
  display: grid;
  box-shadow: 0px 3px 6px #00000099;
`;

const ImgPena1 = styled.img`
  margin-top: 270px;
  margin-left: -50px;
  width: 130px;
  height: 119px;
  position: absolute;
  z-index: 1;

  @media (max-width: 780px) {
    margin-top: 230px;
    margin-left: -20px;
    width: 130px;
    height: 119px;
    position: absolute;
    z-index: 1;
  }
`;

const ImgPena2 = styled.img`
  margin-top: 350px;
  margin-left: -5px;
  width: 96px;
  height: 100px;
  position: absolute;
  z-index: 1;

  @media (max-width: 780px) {
    margin-top: 300px;
    margin-left: -10px;
    width: 100px;
    height: 80px;
    position: absolute;
    z-index: 1;
  }
`;

const ImgPena3 = styled.img`
  margin-top: 180px;
  margin-left: 300px;
  width: 181px;
  height: 155px;
  position: absolute;
  z-index: 1;

  @media (max-width: 780px) {
    margin-top: 180px;
    margin-left: 250px;
    width: 120px;
    height: 100px;
    position: absolute;
    z-index: 1;
  }
`;

const ImgPena4 = styled.img`
  margin-top: 320px;
  margin-left: 360px;
  width: 157px;
  height: 155px;
  position: absolute;
  z-index: 1;

  @media (max-width: 780px) {
    margin-top: 250px;
    margin-left: 250px;
    width: 120px;
    height: 118px;
    position: absolute;
    z-index: 1;
  }
`;

const ImgCard = styled.img`
  opacity: 1;

  padding: 0 20px 0 20px;

  padding-top: 30px;
  /* width: 526px; */
  height: 600px;

  @media (max-width: 780px) {
    width: 375px;
    height: 500px;
  }
`;

const CardText = styled.span`
  color: #f0f0f2;
  font: 100 20px "Open Sans", sans-serif;
  margin: 20px 85px 0 85px;
  width: 271px;
  height: 90px;
  text-align: center;

  @media (max-width: 780px) {
    margin: 30px auto 30px auto;
    height: 80px;
  }
`;

const SpacePersonages = styled.section`
  background: #363636;
  width: auto;
  height: auto;
`;

const SlidePersonages = styled.div`
  padding-bottom: 150px;
  margin: 0 20px 0 20px;
`;

const GroupPersona = styled.div`
  justify-content: center;
  display: flex;
  margin: 0 10px 0 10px;

  @media (max-width: 780px) {
    margin: 0;
  }
`;

const ImgPersonage = styled.img`
  position: absolute;
  z-index: 1;
  margin-top: 100px;
  background-size: 100px;

  @media (max-width: 780px) {
    height: 400px;
    margin-top: 130px;
    width: 300px;
  }
`;

const CardPersonaSuperior = styled.div`
  margin-top: 180px;
  width: 330px;
  height: 450px;
  border-radius: 20%;
  border: 1px solid #fff;
  background: #363636;
  justify-content: center;
  position: absolute;

  @media (max-width: 780px) {
    margin-top: 115px;
    width: 300px;
    height: 410px;
  }
`;

const CardPersona = styled.div`
  margin-top: 299px;
  width: 350px;
  height: 613px;
  border-radius: 27px;
  background: #ffffff;

  @media (max-width: 780px) {
    margin: 200px 10px 0 10px;
  }
`;

const CardPersonaText = styled.span`
  margin: 0 24.5px 0 36.5px;
  height: 80%;
  align-items: flex-end;
  display: flex;
  font: bold 20px "Montserrat", sans-serif;
  color: #000000;
`;

const BaseForm = styled.section`
  height: 742px;
  background: transparent linear-gradient(143deg, #7dede2 0%, #58b790 100%) 0%
    0% no-repeat;
  justify-content: center;
  display: flex;
`;

const BoxForm = styled.div`
  position: absolute;
  margin-top: -45px;
  width: 80%;
  height: 832.5px;
  background: #ffffff 0% 0% no-repeat;
  justify-content: center;
  display: flex;
  border-radius: 10px;

  @media (max-width: 780px) {
    width: 355px;
  }
`;

const FormDimension = styled.div`
  max-width: 1080px;
  width: 745px;

  @media (max-width: 780px) {
    max-width: 745px;
    width: 325px;
  }
`;

const FormTitle = styled.span`
  margin: 82.83px auto 41.02px auto;

  text-align: center;
  color: #63c7a9;
  font: bold 35px "Montserrat", sans-serif;
  justify-content: center;
  display: flex;

  @media (max-width: 780px) {
    margin: 22px auto 20px auto;

    font: bold 20px "Montserrat", sans-serif;
  }
`;

const FormParagraph = styled.p`
  text-align: justify;
  font: 100 20px "Open Sans", sans-serif;

  margin: 20px;
`;

const Form = styled.form`
  display: grid;
  width: 527px;
  margin: 51px auto;

  @media (max-width: 780px) {
    display: grid;
    width: 100%;
    justify-content: center;
    margin: 20px auto;
  }
`;

const BaseInput = styled.div`
  display: flex;
  justify-content: space-between;

  @media (max-width: 780px) {
    display: grid;
    justify-content: space-between;
  }
`;

const Input = styled.input`
  width: 248px;
  height: 48px;

  outline: 0;

  font: 100 18px "Open Sans", sans-serif;
  color: #363636;

  padding-left: 20px;

  @media (max-width: 780px) {
    width: 325px;

    height: 50px;

    margin: 10px 0 5px 0;
  }
`;

const TextArea = styled.textarea`
  margin: 40px auto 0 auto;
  width: 100%;
  height: 197px;

  resize: none;

  outline: 0;

  font: 100 18px "Open Sans", sans-serif;
  color: #363636;

  padding-left: 20px;

  @media (max-width: 780px) {
    width: 325px;
    height: 197px;
    font: 100 14px "Open Sans", sans-serif;
  }
`;

const Button = styled.button`
  margin: 48.79px auto auto 0;

  box-shadow: 0 0 0 0;
  border: 0 none;
  outline: 0;

  width: 248.56px;
  height: 48.21px;
  background: #63c7a9;
  color: #ffffff;
  font: 100 18px "Open Sans", sans-serif;
  cursor: pointer;

  @media (max-width: 780px) {
    margin: 40px 0 0 0;
    width: 100%;
  }
`;

const Footer = styled.footer`
  height: 410px;
  background: #363636;
`;

const IconUp = styled.div`
  width: 70px;
  height: 70px;
  border-radius: 100%;

  margin-top: 98px;
  right: 135px;
  position: absolute;
  background: #ffffff;
  align-items: center;
  justify-content: center;
  display: flex;

  cursor: pointer;

  @media (max-width: 780px) {
    right: 10px;
  }
`;

const StyledIconTrashAlt = styled(FontAwesomeIcon)`
  color: #000;
  margin: 3px;
`;

const Item = styled.footer`
  justify-content: center;
  display: flex;
  margin: 0 10px 0 10px;

  align-items: center;

  width: 1000px;

  font-size: 4em;
`;

const WrapperInput = styled.div`
  display: grid;
`;

const ErrorLabel = styled.label`
  color: #dc3545;
  font: 100 18px "Open Sans", sans-serif;
  margin-left: 10px;
`;

export {
  ContainerFluid,
  Navbar,
  ImgTop,
  NavbarTitle,
  ItensFloat,
  Drag,
  Divisor,
  CardCentral,
  Card,
  ImgPena1,
  ImgPena2,
  ImgPena3,
  ImgPena4,
  ImgCard,
  CardText,
  SpacePersonages,
  SlidePersonages,
  GroupPersona,
  ImgPersonage,
  CardPersonaSuperior,
  CardPersona,
  CardPersonaText,
  BaseForm,
  BoxForm,
  FormDimension,
  FormTitle,
  FormParagraph,
  Form,
  BaseInput,
  Input,
  TextArea,
  Button,
  Footer,
  IconUp,
  StyledIconTrashAlt,
  Item,
  WrapperInput,
  ErrorLabel,
};
