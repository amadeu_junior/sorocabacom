import React from "react";

import { faArrowUp } from "@fortawesome/free-solid-svg-icons";

import Navbar from "../../components/navbar";
import Card from "../../components/card";
import Carousel from "../../components/carousel";
import Formik from "../../components/formik";

import {
  ContainerFluid,
  ItensFloat,
  Drag,
  Divisor,
  CardCentral,
  SpacePersonages,
  SlidePersonages,
  BaseForm,
  BoxForm,
  FormDimension,
  FormTitle,
  FormParagraph,
  Footer,
  IconUp,
  StyledIconTrashAlt,
} from "./home.style.js";

export default function Home() {
  function goTop() {
    window.scrollTo(0, 0);
  }

  return (
    <ContainerFluid>
      <Navbar />
      <ItensFloat>
        <Drag>
          <Divisor>|</Divisor>
        </Drag>
      </ItensFloat>
      <CardCentral>
        <Card />
      </CardCentral>
      <SpacePersonages>
        <SlidePersonages>
          <Carousel />
        </SlidePersonages>
      </SpacePersonages>

      <BaseForm>
        <BoxForm>
          <FormDimension>
            <FormTitle>FORMULÁRIO</FormTitle>
            <FormParagraph>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </FormParagraph>

            <Formik />
          </FormDimension>
        </BoxForm>
      </BaseForm>

      <Footer>
        <IconUp onClick={() => goTop()}>
          <StyledIconTrashAlt icon={faArrowUp} size="2x" color="red" />
        </IconUp>
      </Footer>
    </ContainerFluid>
  );
}
