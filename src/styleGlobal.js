import { createGlobalStyle } from "styled-components";

export const Global = createGlobalStyle`
  * {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: "Niveau";
}

button.rec-arrow-left {
  position: absolute;
  left: 10px;
  background-color: rgba(58, 81, 183, 0.93);
  color: red;
  z-index: 2;
}

button.rec-arrow-right {
  position: absolute;
  right: 10px;
  background-color: rgba(58, 81, 183, 0.93);
  color: red;
  z-index: 2;
}

button.rec-dot_active {
  display: none;
}

div.rec-pagination {
  display: none;
}
`;

export default Global;
